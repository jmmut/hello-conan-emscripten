# Hello Conan Emscripten

## Requirements

### Get compilers and CMake

```
sudo apt-get install build-essential
sudo apt-get install cmake
```

### Get conan

You can install conan in your system with the commands below. It's recommended
to install dependencies in virtualenvs, but I don't want to activate virtualenvs
each time I'm going to do anything with conan, so:

```
sudo apt-get install pip3
sudo pip3 install setuptools
sudo pip3 install conan
```

## Compile

You can either compile the project as a native binary or transpile it to javascript and execute from a browser.

### Native compilation

```
mkdir build-native && cd build-native
conan install ..
conan build ..
```

There should be now a build-binary/bin/myhello that you can execute locally: `./build/bin/myhello`

### Browser compilation

Add the conan profile `emspr` to make conan set up the emscripten environment, needed for the build for browsers.

```
mkdir build-browser && cd build-browser
conan install -pr ../emspr ..
conan build ..
```

There should be now a set of files in build-browser/bin:

- myhello.html
- myhello.js
- myhello.wasm

and you should be able to open myhello.html in your browser, and see the same output the native compilation showed.
