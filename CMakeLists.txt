cmake_minimum_required(VERSION 2.8)
project(MyHello CXX)

include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()


set(SOURCE_FILES
        src/hello.cpp src/hello.h
        )



set(PROGRAM_LIBRARY hellolib)
add_library(${PROGRAM_LIBRARY} ${SOURCE_FILES})
#target_link_libraries(${PROGRAM_LIBRARY} ${CONAN_LIBS})

set(EXECUTABLE_NAME myhello)
add_executable(${EXECUTABLE_NAME} src/main.cpp )
#target_link_libraries(${EXECUTABLE_NAME} ${PROGRAM_LIBRARY})
target_link_libraries(${EXECUTABLE_NAME} ${PROGRAM_LIBRARY} ${CONAN_LIBS})

if(EMSCRIPTEN)
    set_target_properties(${EXECUTABLE_NAME} PROPERTIES
            SUFFIX ".html"
            LINK_FLAGS "--emrun")
endif()
